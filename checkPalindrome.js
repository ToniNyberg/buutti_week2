const palin = process.argv[2];

const palindromeChecker = () => {
    if(palin.split("").reverse().join("") === palin){
        console.log(`Yes, ${palin} is a palindrome `);
    } else {
        console.log(`No, ${palin} is not a palindrome`);
    }
}
palindromeChecker()