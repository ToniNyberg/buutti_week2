const likes = (arr) => {
  if(arr.length === 0){
    return console.log("no likes");
  } else if(arr.length === 1){
    return console.log(`${arr[0]} likes this`);
  } else if(arr.length === 2){
    return console.log(`${arr[0]} and ${arr[1]} like this`);
  } else if(arr.length === 3){
    return console.log(`${arr[0]}, ${arr[1]} and ${arr[2]} like this`);
  } else if (arr.length >= 4){
    return console.log(`${arr[0]}, ${arr[1]}, and ${arr.length - 2} other like this`);
  }
}  


likes(["Alex","Linda","mark","Max","Dille"]);