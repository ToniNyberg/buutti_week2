const arr = [1, 5, 9, 3]

const reducer = (prev, curr) => prev + curr

const aboveAvg = arr.filter(value => value > arr.reduce(reducer) / arr.length)

console.log(aboveAvg);