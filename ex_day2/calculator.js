const calculator = (operator, num1, num2) => {
  switch(operator){
    case "+":
      return num1 + num2
    case "-":
      return num1 - num2
    case "/":
      return num2 === 0 ?
      console.log("cannot divide by 0") :
      num1 / num2
    case "*":
      return num1 * num2
    default:
      console.log("someting went wrong");
    }
  
};

console.log(calculator("/", 2, 3));